<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('index');
});



Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/pages/', 'HomeController');



Route::get('/pages/login', function () {
    return view('login');
});

Route::get('blank','AllReceipts@index');

Route::get('pages/viewall','AllReceipts@index');
Route::post('pages/viewall','AllReceipts@search');


Route::get('pages/addreceipt','AllReceipts@create');

Route::post('pages/addreceipt','AllReceipts@addReceipt');

Route::get('pages/reports','ReportsController');

Route::post('pages/reports','ReportsController@filter');


Route::get('/dologin/','HomeController@login');



Auth::routes();

Route::get('/auth/login', 'HomeController@index');

