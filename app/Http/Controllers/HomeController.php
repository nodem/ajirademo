<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Auth;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/auth/login');
    }


    public function __invoke()
    {
        $receipts = DB::select('select * from receipts');


        $today = DB::select('select sum(amount) as total from receipts where date_of_issue=CURDATE()');

        $yesterday = DB::select('select sum(amount) as total from receipts where date_of_issue = DATE_SUB(CURDATE(), INTERVAL 1 DAY)');

        $thismonth = DB::select('select sum(amount) as total from receipts where Year(date_of_issue) = Year(CURRENT_TIMESTAMP) 
                 AND Month(date_of_issue) = Month(CURRENT_TIMESTAMP)');

        $lastmonth = DB::select('select sum(amount) as total from receipts where   Month(date_of_issue) = Month(CURRENT_TIMESTAMP)-1
AND Year(date_of_issue) = Year(CURRENT_TIMESTAMP) ');


        $tops = DB::select('SELECT SUM(amount) AS \'Total\',issued_by FROM receipts  GROUP BY issued_by order by Total desc limit 5');

        $last7days = DB::select('select SUM(amount) as total from receipts  WHERE date_of_issue >= DATE(NOW()) - INTERVAL 7 DAY order by date_of_issue asc');

        $last30days = DB::select('select SUM(amount) as total from receipts  WHERE date_of_issue >= DATE(NOW()) - INTERVAL 30 DAY order by date_of_issue asc');

        return view('/pages/index',['last30days'=>$last30days[0]->total,'last7days'=>$last7days[0]->total,'tops'=>$tops,'count'=>count($receipts),'today'=>$today[0]->total,'yesterday'=>$yesterday[0]->total,'thismonth'=>$thismonth[0]->total,'lastmonth'=>$lastmonth[0]->total]);

    }


    public function login(Request $request){
        $email = $request->input('email');
        $password = $request->input('password');

        echo $email.' is '.strcasecmp($email,'ajira@admin.com');
        if (strcasecmp($email,'ajira@admin.com')==0 && strcasecmp($password,'admin123')==0) {
            // Authentication passed...
            return redirect()->intended('/pages/');
        }//else
           // return redirect()->intended('/pages/login');

        //$sql = DB::select("select * from users where email='${email}' and password='${password}' ");
    }
}
