<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class AllReceipts extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $receipts = DB::select('select * from receipts order by date_of_issue desc');

        return view('pages/viewall',['receipts'=>$receipts]);
    }



    public function search(Request $request)
    {
        $keyword = $request->input('keyword');

        $receipts = DB::select("SELECT *  FROM receipts  
          where description like '%${keyword}%' or issued_by like '%${keyword}%' or receipt_no like '%${keyword}%'");

    return view('pages/viewall',['receipts'=>$receipts]);
    }



    public function addReceipt(Request $request){

        $receipt_no = $request->input('receipt_no');
        $desc = $request->input('desc');
        $amount = $request->input('amount');

        $issued_by = $request->input('issued_by');

        $date_of_issue = $request->input('date_of_issue');

        DB::insert('insert into receipts (id,receipt_no,description,amount,issued_by,date_of_issue,time_stamp) values(?,?,?,?,?,?,?)',[null,$receipt_no,$desc,$amount,$issued_by,$date_of_issue,null]);

        return view('/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/addreceipt');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request){
        $email = $request->input('email');
        $password = $request->input('pass');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('pages/');
        }

        //  $sql = DB::select("select * from users where email='${email}' and password='${password}' ");
    }
}
