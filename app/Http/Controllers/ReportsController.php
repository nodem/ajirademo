<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;


class ReportsController extends Controller{


    public function __invoke()
    {
        $receipts = DB::select('select * from receipts order by date_of_issue asc');


        $today = DB::select('select sum(amount) as total from receipts where date_of_issue=CURDATE()');

        $yesterday = DB::select('select sum(amount) as total from receipts where date_of_issue = DATE_SUB(CURDATE(), INTERVAL 1 DAY)');

        $thismonth = DB::select('select sum(amount) as total from receipts where Year(date_of_issue) = Year(CURRENT_TIMESTAMP) 
                 AND Month(date_of_issue) = Month(CURRENT_TIMESTAMP)');

        $lastmonth = DB::select('select sum(amount) as total from receipts where   Month(date_of_issue) = Month(CURRENT_TIMESTAMP)-1
AND Year(date_of_issue) = Year(CURRENT_TIMESTAMP) ');


        return view('/pages/reports',['filter'=>'0','label'=>'Your All Time Expenditure','receipts'=>$receipts,'today'=>$today[0]->total,'yesterday'=>$yesterday[0]->total,'thismonth'=>$thismonth[0]->total,'lastmonth'=>$lastmonth[0]->total]);

    }



    public function filter(Request $request){

        $filter = $request->filter;

        if($filter == 0) {
            $receipts = DB::select('select * from receipts order by date_of_issue asc');
            $label = "Your All Time Expenditure";
        }else if($filter == 1) {
            $receipts = DB::select('select * from receipts where Year(date_of_issue) = Year(CURRENT_TIMESTAMP) 
             AND Month(date_of_issue) = Month(CURRENT_TIMESTAMP)');
            $label = "Your Expenditure This Month";
        }else if($filter == 2) {
            $receipts = DB::select('select *  from receipts where Year(date_of_issue) = Year(CURRENT_TIMESTAMP) 
             AND Month(date_of_issue) = Month(CURRENT_TIMESTAMP)');
            $label = "Your Expenditure Last Month";
        }else if($filter == 7) {
            $receipts = DB::select('select * from receipts  WHERE date_of_issue >= DATE(NOW()) - INTERVAL 7 DAY order by date_of_issue asc');
            $label = "Your Expenditure Over The Last 7 days";
        }else if($filter == 30) {
            $receipts = DB::select('select * from receipts  WHERE date_of_issue >= DATE(NOW()) - INTERVAL 30 DAY order by date_of_issue asc');
            $label = "Your Expenditure Over The Last 30 days";
        }

            return view('/pages/reports',['receipts'=>$receipts,'label'=>$label,'filter'=>$filter]);

    }
}