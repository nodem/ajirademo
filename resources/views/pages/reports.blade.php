<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ajira - Receipt Tracker</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <!-- Custom CSS -->
    <link href="{{ asset('/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/pages/">Receipts Tracker</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/pages/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <form role="form" method="post" action="/pages/viewall/">
                                <div class="input-group custom-search-form">


                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                                    <input name="keyword" type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>

                                </div>
                            </form>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="/pages/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>

                        <li>
                            <a href="/pages/addreceipt"><i class="fa fa-plus-square"></i>  Add New Receipt</a>
                        </li>

                        <li>
                            <a href="/pages/viewall"><i class="fa fa-table fa-fw"></i> View All Receipts</a>
                        </li>

                        <li>
                            <a href="/pages/reports"><i class="fa fa-bar-chart-o"></i> Reports </a>
                        </li>


                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">


                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Reports</h1>
                    </div>
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-10 text-center"><h4><?php echo $label; ?></h4></div>
                                    <span class="pull-right" style="padding-right:20px;">
                                        <form role="form" method="post">
                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                        <select  name="filter" onchange="this.form.submit()" class="form-control">

                                            <option  <?php if($filter == 7) echo 'selected'; ?> value="7">Last 7 days</option>
                                            <option  <?php if($filter == 30) echo 'selected'; ?> value="30">Last 30 days</option>
                                            <option  <?php if($filter == 1) echo 'selected'; ?> value="1">This month</option>
                                            <option  <?php if($filter == 2) echo 'selected'; ?>  value="2">Last Month</option>
                                            <option  <?php if($filter == 0) echo 'selected'; ?> value="0" selected>All Time</option>
                                        </select>
                                        </form>
                                    </span>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                                    <div id="morris-area-chart"></div>

                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Flot Charts JavaScript -->
    <script src="{{ asset('/vendor/flot/excanvas.min.js') }}"></script>
    <script src="{{ asset('/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('/vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('/vendor/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('/vendor/flot-tooltip/jquery.flot.tooltip.min.js') }}"></script>


    <!-- Morris Charts JavaScript -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

    <script>
        new Morris.Area({
            // ID of the element in which to draw the chart.
            element: 'morris-area-chart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                //{ year: '2008', value: 20 },
               // { year: '2009', value: 10 },
                //{ year: '2010', value: 5 },
                //{ year: '2011', value: 5 },
               // { year: '2012', value: 20 }

                <?php

                foreach ($receipts as $receipt){
                    $time = strtotime($receipt->date_of_issue)*1000;
                    $amount = $receipt->amount;
                      echo "{ time:${time},amount:${amount} },";
                }
                ?>
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'time',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['amount'],

            xLabels:'day',
            fillOpacity:0.8,
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['You spent KES ']

        });
    </script>




    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('/dist/js/sb-admin-2.js') }}"></script>

</body>

</html>
