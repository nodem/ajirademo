

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ajira - Receipt Tracker</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>


    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/pages/">Receipts Tracker</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/pages/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <form role="form" method="post" action="/pages/viewall/">
                                <div class="input-group custom-search-form">


                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                                    <input name="keyword" type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>

                                </div>
                            </form>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="/pages/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>

                        <li>
                            <a href="/pages/addreceipt"><i class="fa fa-plus-square"></i>  Add New Receipt</a>
                        </li>

                        <li>
                            <a href="/pages/viewall"><i class="fa fa-table fa-fw"></i> View All Receipts</a>
                        </li>

                        <li>
                            <a href="/pages/reports"><i class="fa fa-bar-chart-o"></i> Reports </a>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">

                                    <div class="col-xs-10 text-center">
                                        <h4 >Total Receipts</h4>
                                        <div class="huge"><?php  echo $count; ?></div>

                                    </div>
                                </div>
                            </div>
                            <a href="addreceipt/">
                                <div class="panel-footer">
                                    <span class="pull-left">Add New Receipt</span>
                                    <span class="pull-right"><i class="glyphicon glyphicon-plus-sign"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>

                            <a href="viewall/">
                                <div class="panel-footer">
                                    <span class="pull-left">View All Receipts</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    {{--reports summary--}}
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <h4>Reports Summary</h4>
                            </div>


                            <ul class="list-group">
                                <li class="list-group-item justify-content-between">
                                    Today
                                    <span class="pull-right"><strong><?php  echo 'KES '.number_format($today,2); ?></strong></span>
                                </li>
                                <li class="list-group-item justify-content-between">
                                    Yesterday
                                    <span class="pull-right"><strong><?php  echo 'KES '.number_format($yesterday,2); ?></strong></span>
                                </li>
                                <li class="list-group-item justify-content-between">
                                    Last 7 days
                                    <span class="pull-right"><strong><?php  echo 'KES '.number_format($last7days,2); ?></strong></span>
                                </li>

                                <li class="list-group-item justify-content-between">
                                    Last 30 days
                                    <span class="pull-right"><strong><?php  echo 'KES '.number_format($last30days,2); ?></strong></span>
                                </li>

                                <li class="list-group-item justify-content-between">
                                    This Month
                                    <span class="pull-right"><strong><?php  echo 'KES '.number_format($thismonth,2); ?></strong></span>
                                </li>


                                <li class="list-group-item justify-content-between">
                                    Last Month
                                    <span class="pull-right"> <strong><?php  echo 'KES '.number_format($lastmonth,2); ?> </strong></span>
                                </li>
                            </ul>



                            <a href="/pages/reports">
                                <div class="panel-footer">
                                    <span class="pull-left">View Reports</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>


                    </div>

                    {{--top 10 summary--}}
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">

                                <h4>Top Expenses</h4>
                                <small class="text-muted">Top 5 places you are spending your money</small>
                            </div>


                            <ul class="list-group">

                                @foreach($tops as $i=>$place)
                                    <li class="list-group-item justify-content-between">
                                       <strong><?php echo ++$i.': ';  ?> </strong>  {{$place->issued_by}}
                                        <span class="pull-right"><strong>KES <?php echo number_format($place->Total,2); ?></strong></span>
                                    </li>
                                    @endforeach


                            </ul>



                            <a href="/pages/reports">
                                <div class="panel-footer">
                                    <span class="pull-left">View Reports</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>


                    </div>

                </div>

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('/dist/js/sb-admin-2.js') }}"></script>

</body>

</html>
